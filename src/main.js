var React = require('react');
var ReactDOM = require('react-dom');
//var ReactQuill = require('./react-quill');
var ace = require('brace');
require('brace/mode/html');
require('brace/mode/json');
require('brace/theme/vibrant_ink');

var editor = ace.edit('javascript-editor');
editor.getSession().setMode('ace/mode/html');
editor.getSession().setUseWrapMode(true);
editor.setTheme('ace/theme/vibrant_ink');
editor.setReadOnly(true);

// state machine constants (for the current view)
const VIEW_FORM = 0;
const VIEW_FETCHING = 1;
const VIEW_ERROR = 2;
const VIEW_DISPLAY = 3;

var MainView = React.createClass({
  // initial empty state
  getInitialState: function() {
    return {
      view: VIEW_FORM,              // current state
      url: '',                      // url to fetch html from
      data: '',                     // raw data from url
      searchTags: [],               // tags
      selectedTag: -1,              // current tag index
      selectedTagId: '',            // html id of selected tag
      firsRowId: '',                // id of the first result
      tagTable: []                  // table containing the tags
    };
  },

  // fetches the URL (given by current state)
  // receives a callback function to call on success
  httpGet: function(successCallback, failureCallBack) {
    // defensive check for a function callback
    if (typeof successCallback != 'function') {
      return;
    }

    httpRequest = new XMLHttpRequest();
    // async callback for each state change
    httpRequest.onreadystatechange = function() {
      // checks if the request finished
      if (httpRequest.readyState == XMLHttpRequest.DONE) {
        // checks for success code
        //if (httpRequest.status == 200) {
        if (httpRequest.responseText != null && httpRequest.responseText != '') {
          successCallback(httpRequest.responseText);
        } else {
          failureCallBack();
        }
      }
    }
    var url = this.state.url;
    if (!url.match(/^[a-zA-Z]+:\/\//)) {
      url = 'http://' + url;
    }
    httpRequest.open('GET', url, true);
    httpRequest.send(); 
  },

  // when the user changes the URL, updates the state
  onURLChange: function(event) {
    this.setState({url: event.target.value});
  },

  // when the submit button is pressed, update the view state
  // also calls the fuction to actually fetch the html code
  onFormSubmit: function(event) {
    event.preventDefault();
    this.setState({view: VIEW_FETCHING});
    this.httpGet(this.onURLSuccess, this.onURLFailure);
  },

  // url fetch failure callback
  onURLFailure: function() {
    this.setState({view: VIEW_ERROR});
  },

  // url fetch success callback
  // receives the actual html text as an argument
  onURLSuccess: function(responseText) {
    this.setState({data: responseText});
    this.setState({view: VIEW_DISPLAY});

    // saves all tags and counts
    var readingTag = false;
    var currentTag = '';
    var newTags = [];
    for (var i = 0; i < responseText.length; i++) {
      var ch = responseText.charAt(i);
      if (readingTag) {
        // closes tag
        if ((ch == '>' || ch <= ' ') && currentTag != '') {
          // adds counter
          if (newTags[currentTag] == undefined) {
            newTags[currentTag] = 0;
          }
          newTags[currentTag]++;
          // resets variables
          currentTag = '';
          readingTag = false;
        } else {
          currentTag += ch;
          // crazy characters probably mean wrong parsing
          if (/[^a-zA-Z0-9!/\-]/.test(currentTag)) {
            currentTag = '';
            readingTag = false;
          }
        }
      } else {
        // starts new tag
        if (ch == '<') {
          readingTag = true;
        }
      }
    }

    // sorts the array
    var sortedTags = [];
    var keys = [];
    for (key in newTags) {
      keys.push(key);
    }
    keys.sort(function(a, b) {
      var newA = '';
      var newB = '';
      for (var i = 0; i < a.length; i++) {
        if (a.charAt(i) >= 'A') {
          newA += a.charAt(i);
        }
      }
      for (var i = 0; i < b.length; i++) {
        if (b.charAt(i) >= 'A') {
          newB += b.charAt(i);
        }
      }
      if (newA.toLowerCase() < newB.toLowerCase()) {
        return -1
      } else if (newA.toLowerCase() > newB.toLowerCase()) {
        return 1
      }
      return 0;
    });
    for (key in keys) {
      sortedTags[keys[key]] = newTags[keys[key]];
    }

    // hides spinner and shows editors
    var spinner = document.getElementById('circularG');
    if (spinner != null) {
      spinner.style.display = 'none';
    }
    document.getElementById('javascript-editor').style.display = 'inline-block';
    // sets the data and puts cursor on top
    editor.setValue(this.state.data);
    editor.clearSelection();
    editor.moveCursorTo(0, 0);
    editor.focus();

    var newSearchTags = [];
    var newTagTable = [];
    var i = 0;
    for (key in sortedTags) {
      newSearchTags[i++] = '<' + key;
      rowId = 'row_' + key;
      newTagTable.push(
        <tr id={rowId} onClick={this.onClickTagRow.bind(this, i, rowId)}>
            <td>{i}</td>
            <td>{'<' + key + '>'}</td>
            <td>{sortedTags[key]}</td>
        </tr>
      );
      if (i == 1) {
        this.setState({firstRowId: rowId});
      }
    }
    this.setState({searchTags: newSearchTags});
    this.setState({tagTable: newTagTable});
  },

  // callback to clicking on a tag
  onClickTagRow: function(tagRow, rowId) {
    this.searchTag(tagRow-1, rowId);
    document.getElementById(rowId).bgColor = '#5182bb';
  },

  // performs a search given a selectedTag (index of tags)
  // if it's the same index, just find the next one
  searchTag: function(newSelectedTag, newTagId) {
    if (newSelectedTag == 'prev' && newTagId < 0) {
      // be defensive to avoid invalid state
      if (this.state.selectedTag >= 0) {
        editor.findPrevious();
      } else {
        this.onClickTagRow(1, this.state.firstRowId);
      }
    } else if (newSelectedTag == 'next' && newTagId < 0) {
      // be defensive to avoid invalid state
      if (this.state.selectedTag >= 0) {
        editor.findNext();
      } else {
        this.onClickTagRow(1, this.state.firstRowId);
      }
    } else if (newSelectedTag == this.state.selectedTag) {
      editor.findNext();
    } else {
      var regexp = '(' + this.state.searchTags[newSelectedTag] + '>|' + this.state.searchTags[newSelectedTag] + ' )'; 
      editor.find(regexp, {
        backwards: false,
        wrap: true,
        caseSensitive: false,
        wholeWord: false,
        regExp: true,
      });
      // unsets bgcolor of previously selected row
      var oldRow = document.getElementById(this.state.selectedTagId);
      if (oldRow != null) {
        oldRow.bgColor = 'transparent';
      }
      // update state
      this.setState({selectedTag: newSelectedTag});
      this.setState({selectedTagId: newTagId});
    }
  },

  createTagTable: function() {
  },

  render: function() {
    // initial state with search bar
    if (this.state.view == VIEW_FORM) {
      return (
        React.createElement('div', {},
          React.createElement('div', {className: 'page-title'},
            React.createElement('h1', {}, 'Enter URL')
          ),
          React.createElement('form', {
              onSubmit: this.onFormSubmit,
              className: 'form-wrapper',
            },
            React.createElement('input', {
              type: 'text',
              id: 'search',
              placeholder: 'e.g. facebook.github.io/react/',
              onChange: this.onURLChange,
            }),
            React.createElement('input', {
              type: 'submit',
              id: 'submit',
              value: 'Fetch!',
            })
          )
        )
      );
    // fetching state, presents a spinner while here
    } else if (this.state.view == VIEW_FETCHING) {
      return (
        React.createElement('div', {className: 'spinner'},
          React.createElement('div', {id: 'circularG'},
            React.createElement('div', {className: 'circularG', id:'circularG_1'}),
            React.createElement('div', {className: 'circularG', id:'circularG_2'}),
            React.createElement('div', {className: 'circularG', id:'circularG_3'}),
            React.createElement('div', {className: 'circularG', id:'circularG_4'}),
            React.createElement('div', {className: 'circularG', id:'circularG_5'}),
            React.createElement('div', {className: 'circularG', id:'circularG_6'}),
            React.createElement('div', {className: 'circularG', id:'circularG_7'}),
            React.createElement('div', {className: 'circularG', id:'circularG_8'})
          )
        )
      );
    // error view
    } else if (this.state.view == VIEW_ERROR) {
      return (
        <div className='errorPage'>
          <h1>ERROR!</h1>
          <h2>{'URL "' + this.state.url + '" failed to load. Please try again.'}</h2>
          <a className = "btnBackError" href = "">Back</a>
        </div>
      );
    // main display view for the html text
    } else if (this.state.view == VIEW_DISPLAY) {
      var tableHead =
        <thead>
          <tr>
            <th>#</th>
            <th>Tag</th>
            <th>Total</th>
          </tr>
        </thead>;

      var tableBody = 
        <tbody>
          {this.state.tagTable}
        </tbody>;

      var backButton =
        <a className = "btnBack" href = "">&lt; Back</a>;
      var prevButton =
        <a className = "btnPrev" onClick={this.searchTag.bind(this, 'prev', -1)}>Prev</a>;
      var nextButton =
        <a className = "btnNext" onClick={this.searchTag.bind(this, 'next', -1)}>Next</a>;

      var viewDisplay = React.createElement('div', {
          id: 'tags-editor',
          className: 'tags',
          align: 'left'
        },
        backButton,
        prevButton,
        nextButton,
        React.createElement('table', {id: 'table-tag'},
          tableHead,
          tableBody
        )
      );

      return viewDisplay;
    }
  },
});

var rootElement = React.createElement(MainView, {});
ReactDOM.render(rootElement, document.getElementById('root'));

