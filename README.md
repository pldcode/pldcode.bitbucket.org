pdcode.github.io
================

# Screenshots

![Initial screen](screenshots/pdcode01.png?raw=true "Initial screen")
![Context and source view](screenshots/pdcode02.png?raw=true "Context and source view")
![Searching for a tag](screenshots/pdcode03.png?raw=true "Searching for a tag")
![Searching for another tag](screenshots/pdcode04.png?raw=true "Searching for another tag")
![Error screen](screenshots/pdcode05.png?raw=true "Error screen")

# More Info

This was built using basically with Javascript and CSS. I used React, Browserify and Ace. The usage is basic providing an URL for the app, it retrieves the html code and displays in separate view, with a summary view of all the tags. The tags are sorted and it is possible to go through them by clicking on their row in the summary (repeated clicks go to the next occurrence) or by clicking on "prev" and "next" buttons. All tag matches searched are highlighted in the editor view. The "back" button returns to the main view.

Notice that due to cross-origin permissions, this wll not work on Chrome (unless the check is turned off). Also due to restrictions on github (also for cross-origin), it may no be possible to fetch pages outside of github.io domain. Both Firefox and Safari should work well with github.io pages. 

