SOURCE = src/main.js
TARGET = dist/bundle.js
BROWSERIFY = browserify
FLAGS = -t [ babelify --presets [ react ] ]

all:
	$(BROWSERIFY) $(FLAGS) $(SOURCE) -o $(TARGET)

clean:
	rm -f $(TARGET)

